﻿using System;
using System.Collections.Generic;

namespace PlayerPosition
{
    internal class ConsolePrinter
    {
        private readonly Dictionary<Player, Vector2> _printedPlayers = new ();

        public void PrintPlayer(Player player)
        {
            if (_printedPlayers.ContainsKey(player) == false)
            {
                _printedPlayers.Add(player, player.Position);
            }
            else
            {
                Console.SetCursorPosition(_printedPlayers[player].X, _printedPlayers[player].Y);
                Console.Write(' ');
            }

            Console.SetCursorPosition(player.Position.X, player.Position.Y);
            Console.Write('@');

            _printedPlayers[player] = player.Position;
        }
    }
}
