﻿using System;

namespace PlayerPosition
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.CursorVisible = false;

            var defaultPosition = new Vector2(2, 2);

            var player = new Player(defaultPosition);

            var consolePrinter = new ConsolePrinter();
            consolePrinter.PrintPlayer(player);
        }
    }
}