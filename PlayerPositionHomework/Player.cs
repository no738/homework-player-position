﻿using System;

namespace PlayerPosition
{
    public class Player
    {
        private readonly Vector2 _position;

        public Player(Vector2 defaultPosition)
        {
            Position = defaultPosition;
        }

        public Vector2 Position
        {
            get => _position;
            private init
            {
                if (value.X < 0 || value.Y < 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(value));
                }
                
                _position = value;
            }
        }
    }
}