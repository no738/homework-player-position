﻿namespace PlayerPosition
{
    public readonly struct Vector2
    {
        public Vector2(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; }
        public int Y { get; }

        public override string ToString()
        {
            return $"({X}, {Y})";
        }

        public override bool Equals(object? obj) => obj is Vector2 other && this.Equals(other);

        public override int GetHashCode() => (X, Y).GetHashCode();

        public bool Equals(Vector2 p) => X == p.X && Y == p.Y;

        public static bool operator ==(Vector2 firstVector2, Vector2 secondVector2) => firstVector2.Equals(secondVector2);

        public static bool operator !=(Vector2 firstVector2, Vector2 secondVector2) => !firstVector2.Equals(secondVector2);
    }
}